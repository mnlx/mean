var mongoose = require('mongoose');

var greatSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  howGay: Number,
});

var locationSchema = new mongoose.Schema({
  name: String,
  isGay: Boolean,
  howGay: { type: Number, default: 10 },
  friendsList: [greatSchema],
  coords: { type: [Number], index: '2dsphere' },
});

mongoose.model('Location', locationSchema);
