var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/Loc8r';
mongoose.connect(dbURI);

mongoose.connection.on('connected', function() {
  console.log('Connected to db: ' + dbURI);
});

mongoose.connection.on('error', function(err) {
  console.log('Error o ndb: ' + err);
});

mongoose.connection.on('disconnected', function() {
  console.log('Disconnected db: ' + dbURI);
});

var gracefullShutdown = function(msg, callback) {
  mongoose.connection.close(function() {
    console.log('mongoose disconnected through ' + msg);
    callback();
  });
};

process.once('SIGUSR2', function() {
  gracefullShutdown('nodemon retart', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

process.on('SIGINT', function() {
  gracefulShutdown('app termination', function() {
    process.exit(0);
  });
});

process.on('SIGTERM', function() {
  gracefulShutdown('Heroku app shutdown', function() {
    process.exit(0);
  });
});
